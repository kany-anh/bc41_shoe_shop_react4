import logo from "./logo.svg";
import "./App.css";
import Shoe_Shop_React from "./Shoe_Shop_React/Shoe_Shop_React";

function App() {
  return (
    <div className="App">
      <Shoe_Shop_React />
    </div>
  );
}

export default App;
