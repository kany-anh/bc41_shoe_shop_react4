import React, { Component } from "react";
import Item_Shoe from "./Item_Shoe";

class List_Shoe extends Component {
  renderListShoe = () => {
    return this.props.list.map((item, index) => {
      return (
        <Item_Shoe handleAdd2={this.props.handleAdd1} key={index} data={item} />
      );
    });
  };
  render() {
    return (
      <div>
        <h2 className="my-2">List Shoe</h2>
        <div className="row">{this.renderListShoe()}</div>
      </div>
    );
  }
}

export default List_Shoe;
