import React, { Component } from "react";

class Item_Shoe extends Component {
  render() {
    let { name, price, image } = this.props.data;
    return (
      <div className="card col-3">
        <img src={image} className="card-img-top" alt="" />
        <div className="card-body">
          <h5 className="card-title">{name}</h5>
          <p className="card-text">{price}</p>
          <a
            onClick={() => {
              this.props.handleAdd2(this.props.data);
            }}
            href="#"
            className="btn btn-primary"
          >
            Add
          </a>
        </div>
      </div>
    );
  }
}

export default Item_Shoe;
