import React, { Component } from "react";

class Cart_Shoe extends Component {
  renderListTable = () => {
    return this.props.cart.map((item, index) => {
      return (
        <tr key={index}>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>
            <button
              onClick={() => {
                this.props.handleGainQuantity(item.id, -1);
              }}
              className="btn btn-warning mx-1"
            >
              -
            </button>
            {item.soLuong}
            <button
              onClick={() => {
                this.props.handleGainQuantity(item.id, 1);
              }}
              className="btn btn-success mx-1"
            >
              +
            </button>
          </td>
          <td>{item.price * item.soLuong}</td>
          <td>
            <img style={{ width: 50 }} src={item.image} alt="" />
          </td>
          <td>
            <button
              onClick={() => {
                this.props.handleDelete(item.id);
              }}
              className="btn btn-danger
            "
            >
              Delete
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div className="mt-5">
        <table className="table">
          <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Quantity</th>
              <th>Price</th>
              <th>Img</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>{this.renderListTable()}</tbody>
        </table>
      </div>
    );
  }
}

export default Cart_Shoe;
